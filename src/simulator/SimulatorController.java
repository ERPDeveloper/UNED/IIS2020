/*
 * Copyright (C) 2020 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package simulator;

import tools.Controller;
import tools.MenuItem;
import tools.Utilities;
import township.Country;

/**
 * Controller of actions for countries and townships.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - April 2020
 */
public class SimulatorController extends Controller {

    /**
     * Constants for actions
     */
    private static final int ACTION_RUN = 1;

    /*
     * Constants for menu options
     */
    private static final String MENU_TEXT_RUN = "Nueva Simulación";

    /*
     * Constants for request
     */
    private static final String TEXT_SELECT_NUMDAYS = "Seleccione número de días (0 - Cancelar):";

    /**
     * Constants for user message
     */
    private static final String TEXT_NO_TOWNSHIP = "No hay comunidades definidas";

    /**
     * Link to country data
     */
    private final Country country;

    /**
     * Class constructor
     *
     * @param country
     */
    public SimulatorController(Country country) {
        super();
        this.country = country;
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    @Override
    public boolean execOption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_RUN:
                this.actionRun();
                break;
        }

        return true;
    }

    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    @Override
    public MenuItem getMenu() {
        MenuItem item = new MenuItem(MENU_TEXT_RUN, ACTION_RUN, this);
        return item;
    }

    /**
     * Request num days, and run simulator.
     */
    private void actionRun() {
        if (this.country.size() == 0) {
            System.out.println(TEXT_NO_TOWNSHIP);
            return;
        }

        int days = Utilities.requestInt(TEXT_SELECT_NUMDAYS, 0, 30);
        if (days == 0) {
            return;
        }

        Simulator simulator = new Simulator(this.country);
        simulator.simulationForDays(days);
    }
}
