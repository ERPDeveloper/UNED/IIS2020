/*
 * Copyright (C) 2020 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package simulator;

import java.util.Formatter;
import tools.Utilities;
import township.Country;

/**
 * Class for process virus simulator.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - April 2020
 */
public class Simulator {

    private static final String TEXT_HEADER = "%-20s\t%10s\t%10s\t%8s\t%8s\t%15s";
    private static final String TEXT_DAY = "Día";
    private static final String TEXT_POPULATION = "Población";
    private static final String TEXT_INFECTED = "Infectados";
    private static final String TEXT_PCTINFECTED = "% Inf.";
    private static final String TEXT_NEWINFECTED = "Nuevos Local";
    private static final String TEXT_NEWCOMMUTER = "Por Movilidad";

    /**
     * Link to country data.
     */
    private final Country country;

    /**
     * Class constructor.
     *
     * @param country
     */
    public Simulator(Country country) {
        this.country = country;
    }

    /**
     * Run a simulator for indicated days.
     *
     * @param days
     */
    public void simulationForDays(int days) {
        this.firstDay();

        for (int i = 2; i <= days; i++) {
            this.country.calculateInfection();
            this.showData(i);
        }
    }

    /**
     * Start a new infection into random township.
     */
    private void firstDay() {
        int townshipInfected = Utilities.randomInt(1, this.country.size()) - 1;
        this.country.clearInfection();
        this.country.updateInfected(townshipInfected, 1, 0);
        this.showData(1);
    }

    /**
     * Display Infection data.
     *
     * @param day
     */
    private void showData(int day) {
        Formatter header = new Formatter();
        header.format(TEXT_HEADER,
                TEXT_DAY + " " + day,
                TEXT_POPULATION,
                TEXT_INFECTED,
                TEXT_PCTINFECTED,
                TEXT_NEWINFECTED,
                TEXT_NEWCOMMUTER
        );

        System.out.println(header.toString());
        System.out.println(Utilities.getLine('-', 105));
        System.out.print(this.country.showInfection());
        System.out.println(Utilities.getLine('-', 105));
        System.out.println(this.country.showTotalInfection());
        System.out.println("");
        System.out.println("");
    }
}
