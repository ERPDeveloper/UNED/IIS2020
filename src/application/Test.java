/*
 * Copyright (C) 2020 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import township.Country;
import township.Township;

/**
 * Virus Simulator application. Test Class.
 * Simulation of the spread of a virus in a populated territory.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - April 2020
 */
public class Test {

    private static final int TESTFIELD_NAME = 0;
    private static final int TESTFIELD_POPULATION = 1;
    private static final int TESTFIELD_COEFFICIENT_E = 2;
    private static final int TESTFIELD_COEFFICIENT_P = 3;

    private static final String TOWNSHIP_FILENAME = "townships.dat";

    /**
     * Add Township to Country
     *
     * @param country
     */
    public void addTownships(Country country) {
        for (String data : this.dataFromFileName(TOWNSHIP_FILENAME)) {
            Township township = this.getTownshipsFromData(data);
            country.addTownship(township);
        }
    }

    /**
     * Load data into array list from file text.
     *
     * @param fileName
     * @return ArrayList
     */
    private ArrayList<String> dataFromFileName(String fileName) {
        InputStream stream = getClass().getResourceAsStream(fileName);
        if (stream == null) {
            System.err.println("No se puede abrir el fichero de comunidades.\n");
            return new ArrayList<>();
        }

        ArrayList<String> result = new ArrayList<>();
        try {
            Reader reader = new InputStreamReader(stream, "UTF-8");
            BufferedReader file = new BufferedReader(reader);
            String data = file.readLine();
            while (data != null) {
                if (data.charAt(0) != '#') {
                    result.add(data);
                }
                data = file.readLine();
            }
            file.close();
            reader.close();
            stream.close();
        }
        catch (IOException exception) {
            System.err.println(exception.getMessage() + "\n");
        }

        return result;
    }

    /**
     * Get new Township setting from readed data
     * @param data
     * @return Township
     */
    private Township getTownshipsFromData(String data) {
        Township result = new Township();
        String[] values = data.split(",");

        result.setName(values[TESTFIELD_NAME]);
        result.setPopulation(Integer.parseInt(values[TESTFIELD_POPULATION]));
        result.setCoefficientE(Integer.parseInt(values[TESTFIELD_COEFFICIENT_E]));
        result.setCoefficientP(Integer.parseInt(values[TESTFIELD_COEFFICIENT_P]));

        return result;
    }
}
