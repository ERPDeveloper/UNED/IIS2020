/*
 * Copyright (C) 2020 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package application;

import simulator.SimulatorController;
import tools.Menu;
import township.Country;
import township.TownshipController;

/**
 * Virus Simulator application.
 * Simulation of the spread of a virus in a populated territory.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - April 2020
 */
public class Main {

    private static final String MENU_TEXT_CAPTION = "Menú Principal";

    /**
     * Main action. Application start.
     *
     * @param args
     */
    public static void main(String[] args) {
        // Create main data
        Country country = new Country();

        /// Create main options controllers
        TownshipController township = new TownshipController(country);
        SimulatorController simulator = new SimulatorController(country);

        // Load test data, if first param is "test"
        if (args.length > 0 && "test".equals(args[0])) {
            Test test = new Test();
            test.addTownships(country);
        }

        // Create main menu
        Menu menu = new Menu();
        menu.addItem(township.getMenu());
        menu.addItem(simulator.getMenu());

        // Execute main menu
        menu.execute(MENU_TEXT_CAPTION, null);
    }
}
