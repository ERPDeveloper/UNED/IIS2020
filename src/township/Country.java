/*
 * Copyright (C) 2020 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package township;

import java.util.ArrayList;

/**
 * Class for manage township list of country.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - April 2020
 */
public class Country {

    /**
     * Constants for display messages
     */
    private static final String TEXT_TOTAL = "Total";

    /**
     * Constants for user message
     */
    private static final String TEXT_NO_TOWNSHIP = "No hay comunidades definidas";

    /**
     * Total data of people into country.
     */
    private final Township totals;

    /**
     * Township list of country.
     */
    private final ArrayList<Township> townshipList;

    /**
     * Class constructor.
     */
    public Country() {
        this.townshipList = new ArrayList<>();
        this.totals = new Township();
        this.totals.setName(TEXT_TOTAL);
    }

    /**
     * Add a new township to list.
     *
     * @param township
     * @return int
     */
    public int addTownship(Township township) {
        if (this.townshipList.add(township)) {
            this.updatePopulation(township.getPopulation());
            return this.townshipList.size() - 1;
        }
        return -1;
    }

    /**
     * Calculate statistical infection for one day.
     */
    public void calculateInfection() {
        for (Township township : this.townshipList) {
            int populationInfected = township.calculateInfection();
            int commuterInfected = this.calculateCommuterInfection(township);
            this.updateInfected(township, populationInfected, commuterInfected);
        }
    }

    /**
     * Calculate statistical infectation for daily commuters of other townships.
     *
     * @param target
     * @return int
     */
    private int calculateCommuterInfection(Township source) {
        int count = this.size() - 1; // Number of townships where to distribute the travelers
        if (count < 1) {
            return 0;
        }

        int dailyCommuter = source.getCoefficientV() / count; // Number of daily commuter from source to target
        int infected = 0;
        for (Township target : this.townshipList) {
            if (target == source || target.getInfected() == 0) {
                continue;
            }

            float coef = (float) target.getCoefficient(); // Coefficient of infection on target township
            float sourceInfected = target.getInfected() / target.getPopulation(); // Percentage target infected
            float commuterInfected = sourceInfected * coef * dailyCommuter; // Infected source people into target township
            infected += (commuterInfected > dailyCommuter) ? dailyCommuter : commuterInfected;
        }
        return infected;
    }

    /**
     * Clear all statistical data.
     */
    public void clearInfection() {
        for (Township township : this.townshipList) {
            township.clearInfection();
        }
        this.totals.clearInfection();
    }

    /**
     * Get a township from list.
     *
     * @param index
     * @return Township
     */
    public Township getTownship(int index) {
        return this.townshipList.get(index);
    }

    /**
     * Remove township at index from township list.
     *
     * @param index
     */
    public void removeTownship(int index) {
        Township township = this.townshipList.remove(index);
        this.updatePopulation(township.getPopulation() * -1);
    }

    /**
     * Infection description formatted for printing.
     *
     * @return String
     */
    public String showInfection() {
        String text = "";
        for (Township township : this.townshipList) {
            if (township.hasInfection()) {
                text += township.showInfection() + "\n";
            }
        }
        return text;
    }

    /**
     * Total infection description formatted for printing.
     *
     * @return String
     */
    public String showTotalInfection() {
        this.totals.updateInfected(0, 0);
        return this.totals.showInfection();
    }

    /**
     * Get number of township into list.
     *
     * @return int
     */
    public int size() {
        return this.townshipList.size();
    }

    /**
     * Description of townships list
     *
     * @return String
     */
    @Override
    public String toString() {
        if (this.size() == 0) {
            return TEXT_NO_TOWNSHIP;
        }

        String text = "";
        int index = 1;
        for (Township township : this.townshipList) {
            text += index + ": \t" + township.toString() + "\n";
            ++index;
        }
        return text;
    }

    /**
     * Update total infected for indicated township.
     *
     * @param index
     * @param population
     * @param commuter
     */
    public void updateInfected(int index, int population, int commuter) {
        Township township = this.getTownship(index);
        this.updateInfected(township, population, commuter);
    }

    /**
     * Update total infected for indicated township.
     *
     * @param township
     * @param value
     */
    private void updateInfected(Township township, int population, int commuter) {
        township.updateInfected(population, commuter);
        this.totals.updateInfected(population, commuter);
    }

    /**
     * Update total population.
     *
     * @param value
     */
    public void updatePopulation(int value) {
        int population = this.totals.getPopulation();
        this.totals.setPopulation(population + value);
    }
}