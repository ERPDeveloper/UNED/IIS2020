/*
 * Copyright (C) 2020 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package township;

import tools.Controller;
import tools.MenuItem;
import tools.Utilities;

/**
 * Controller of actions for countries and townships.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - April 2020
 */
public class TownshipController extends Controller {

    /**
     * Constants for actions
     */
    private static final int ACTION_ADD = 1;
    private static final int ACTION_DEL = 3;
    private static final int ACTION_LIST = 2;

    /*
     * Constants for menu options
     */
    private static final String MENU_TEXT_CAPTION = "Comunidades";

    private static final String MENU_TEXT_ADD = "Nueva Comunidad";
    private static final String MENU_TEXT_DEL = "Eliminar Comunidad";
    private static final String MENU_TEXT_LIST = "Ver/Modificar Comunidad";

    /*
     * Constants for request
     */
    private static final String TEXT_SELECT_TOWNSHIP = "Seleccione comunidad (0 - Cancelar):";

    /**
     * Link to country data
     */
    private final Country country;

    /**
     * Class constructor
     *
     * @param country
     */
    public TownshipController(Country country) {
        super();
        this.country = country;
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    @Override
    public boolean execOption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_ADD:
                this.actionAdd();
                break;

            case ACTION_DEL:
                this.actionRemove();
                break;

            case ACTION_LIST:
                this.actionList();
                break;
        }

        return true;
    }

    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    @Override
    public MenuItem getMenu() {
        MenuItem item = new MenuItem(MENU_TEXT_CAPTION, 0, this);
        item.addChildren(MENU_TEXT_ADD, ACTION_ADD);
        item.addChildren(MENU_TEXT_LIST, ACTION_LIST);
        item.addChildren(MENU_TEXT_DEL, ACTION_DEL);
        return item;
    }

    /**
     * Create a new township from user data and add to township list.
     */
    private void actionAdd() {
        Township township = new Township();
        TownshipFormController controller = new TownshipFormController(township);
        if (controller.requestForm()) {
            this.country.addTownship(township);
            controller.execMenu();
        }
    }

    /**
     * List the township list, and ask to user for one to mofify.
     */
    private void actionList() {
        int townshipIndex = this.requestTownship() - 1;
        if (townshipIndex < 0) {
            return;
        }

        Township township = this.country.getTownship(townshipIndex);
        int population = township.getPopulation();

        TownshipFormController controller = new TownshipFormController(township);
        controller.execMenu();

        if (population != township.getPopulation()) {
            this.country.updatePopulation(population - township.getPopulation());
        }
    }

    /**
     * Remove the township indicated by the user from the list
     */
    private void actionRemove() {
        int townshipIndex = this.requestTownship() - 1;
        if (townshipIndex < 0) {
            return;
        }
        this.country.removeTownship(townshipIndex);
    }

    /**
     * Display the list of township and request a user
     *
     * @return int
     */
    private int requestTownship() {
        System.out.println(this.country.toString());
        int size = this.country.size();
        return (size > 0)
            ? Utilities.requestInt(TEXT_SELECT_TOWNSHIP, 0, size)
            : 0;
    }
}
