/*
 * Copyright (C) 2020 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package township;

import java.text.NumberFormat;
import java.util.Locale;
import tools.Controller;
import tools.MenuItem;
import tools.Utilities;

/**
 * Controller of actions for township form data.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - April 2020
 */
public class TownshipFormController extends Controller {

    /*
     * Constants for actions
     */
    private static final int ACTION_NAME = 1;
    private static final int ACTION_POPULATION = 2;
    private static final int ACTION_COEF_E = 3;
    private static final int ACTION_COEF_P = 4;

    /*
     * Constants for menu options
     */
    private static final String MENU_TEXT_CAPTION = "Modificación de comunidad";
    private static final String MENU_TEXT_COEF_E = "Modificar coef. E";
    private static final String MENU_TEXT_COEF_P = "Modificar coef. P";
    private static final String MENU_TEXT_NAME = "Modificar nombre";
    private static final String MENU_TEXT_POPULATION = "Modificar población";

    /*
     * Constants for request
     */
    private static final String TEXT_NEW_NAME = "Introducir nuevo nombre:";
    private static final String TEXT_NEW_POPULATION = "Introducir nueva población:";
    private static final String TEXT_NEW_COEF_E = "Introducir nuevo coeficiente contacto no infectados:";
    private static final String TEXT_NEW_COEF_P = "Introducir nuevo coeficiente probabilidad de contagio:";

    /**
     * Township on whom changes are made
     */
    private final Township township;

    /**
     * Constructor
     *
     * @param township
     */
    public TownshipFormController(Township township) {
        super();
        this.township = township;
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    @Override
    public boolean execOption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_COEF_E:
                this.actionModifyCoefE();
                break;

            case ACTION_COEF_P:
                this.actionModifyCoefP();
                break;

            case ACTION_NAME:
                this.actionModifyName();
                break;

            case ACTION_POPULATION:
                this.actionModifyPopulation();
                break;
        }

        return true;
    }

    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    @Override
    public MenuItem getMenu() {
        MenuItem item = new MenuItem(MENU_TEXT_CAPTION, 0, this);
        item.addChildren(MENU_TEXT_NAME, ACTION_NAME);
        item.addChildren(MENU_TEXT_POPULATION, ACTION_POPULATION);
        item.addChildren(MENU_TEXT_COEF_E, ACTION_COEF_E);
        item.addChildren(MENU_TEXT_COEF_P, ACTION_COEF_P);
        return item;
    }

    /**
     * Returns an optional caption for an item menu
     *
     * @param menuItem
     * @return String
     */
    @Override
    public String getOptionCaption(MenuItem menuItem) {
        NumberFormat numberFormat;
        switch (menuItem.getOption()) {
            case ACTION_NAME:
                return this.township.getName();

            case ACTION_POPULATION:
                numberFormat = NumberFormat.getIntegerInstance(new Locale("es", "ES"));
                return numberFormat.format(this.township.getPopulation());

            case ACTION_COEF_E:
                numberFormat = NumberFormat.getNumberInstance(new Locale("es", "ES"));
                return numberFormat.format(this.township.getCoefficientE()) + "%";

            case ACTION_COEF_P:
                numberFormat = NumberFormat.getNumberInstance(new Locale("es", "ES"));
                return numberFormat.format(this.township.getCoefficientP()) + "%";
        }
        return super.getOptionCaption(menuItem);
    }

    /**
     * It asks the user to enter the data.
     * It is canceled:
     *   - if the name is left blank
     *   - if zero is entered in any numerical value
     *
     * @return boolean
     */
    public boolean requestForm() {
        return this.actionModifyName()
            && this.actionModifyPopulation()
            && this.actionModifyCoefE()
            && this.actionModifyCoefP();
    }

    /**
     * The user requests a new value for the coefficient E.
     *
     * @return boolean
     */
    private boolean actionModifyCoefE() {
        int coef = Utilities.requestInt(TEXT_NEW_COEF_E, 0, 99);
        if (coef > 0) {
            this.township.setCoefficientE(coef);
            return true;
        }
        return false;
    }

    /**
     * The user requests a new value for the coefficient P.
     *
     * @return boolean
     */
    private boolean actionModifyCoefP() {
        int coef = Utilities.requestInt(TEXT_NEW_COEF_P, 0, 99);
        if (coef > 0) {
            this.township.setCoefficientP(coef);
            return true;
        }
        return false;
    }

    /**
     * The user requests a new value for the name.
     *
     * @return boolean
     */
    private boolean actionModifyName() {
        String name = Utilities.requestString(TEXT_NEW_NAME, 0);
        if (name.isEmpty()) {
            return false;
        }
        this.township.setName(name);
        return true;
    }

    /**
     * The user requests a new value for the population.
     *
     * @return boolean
     */
    private boolean actionModifyPopulation() {
        int population = Utilities.requestInt(TEXT_NEW_POPULATION, 0, 0);
        if (population > 0) {
            this.township.setPopulation(population);
            return true;
        }
        return false;
    }
}
