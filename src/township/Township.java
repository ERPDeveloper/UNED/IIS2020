/*
 * Copyright (C) 2020 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package township;

import java.util.Formatter;

/**
 * Class for manage each one of the township of country.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - April 2020
 */
public class Township {

    /**
     * Constants for display messages
     */
    private static final String TEXT_FORMAT_INFECTION = "%-20s\t%10s\t%10s\t%8s\t%8s\t%15s";
    private static final String TEXT_FORMAT_TOSTRING = "%-20s\tPoblación: %9s\tViajeros: %7s\tContacto No Inf.: %d%%\tProb. Contagio: %d%%";

    /*
     * Constants
     */
    private static final int PERCENTAGE_MOVING = 5;

    /**
     * Number of contacts with uninfected people.
     */
    private int coefficientE;

    /**
     * Probability of infection.
     */
    private int coefficientP;

    /**
     * Number of daily commuters.
     */
    private int coefficientV;

    /**
     * Total of population infected.
     */
    private int infected;

    /**
     * Name of township
     */
    private String name;

    /**
     * Number of population infected last day from other township.
     */
    private int newCommuter;

    /**
     * Number of population infected last day.
     */
    private int newInfected;

    /**
     * Percentage of population infected.
     */
    private double pctInfected;

    /**
     * Percentage of population commuters by day.
     */
    private final int pctMoving;

    /**
     * Number of person into township
     */
    private int population;

    /**
     * Class constructor
     */
    public Township() {
        this.name = "";
        this.pctMoving = Township.PERCENTAGE_MOVING;
    }

    /**
     *
     * @return int
     */
    public int calculateInfection() {
        double infection = this.infected * this.getCoefficient();
        if ((this.infected + infection) > this.population) {
            return this.population - this.infected;
        }
        return (int) infection;
    }

    /**
     * Clear Infection fields data.
     */
    public void clearInfection() {
        this.infected = 0;
        this.pctInfected = 0.00;
        this.newInfected = 0;
        this.newCommuter = 0;
    }

    /**
     * Infectation coefficient by contact with the population.
     *
     * @return
     */
    public double getCoefficient() {
        return this.coefficientE * this.coefficientP / 100.00;
    }

    /**
     * Get number of contacts with uninfected people.
     *
     * @return int
     */
    public int getCoefficientE() {
        return this.coefficientE;
    }

    /**
     * Get probability of infection.
     *
     * @return int
     */
    public int getCoefficientP() {
        return this.coefficientP;
    }

    /**
     * Get number of daily commuters.
     *
     * @return int
     */
    public int getCoefficientV() {
        return coefficientV;
    }

    /**
     * Get total of population infected.
     *
     * @return int
     */
    public int getInfected() {
        return infected;
    }

    /**
     * Get name of township.
     *
     * @return String
     */
    public String getName() {
        return this.name;
    }

    /**
     * Get number of person into township.
     *
     * @return int
     */
    public int getPopulation() {
        return this.population;
    }

    /**
     * Say if township has population infected.
     *
     * @return boolean
     */
    public boolean hasInfection() {
        return this.infected > 0;
    }

    /**
     * Set number of contacts with uninfected people.
     *
     * @param coefficientE
     */
    public void setCoefficientE(int coefficientE) {
        this.coefficientE = coefficientE;
    }

    /**
     * Set probability of infection.
     *
     * @param coefficientP
     */
    public void setCoefficientP(int coefficientP) {
        this.coefficientP = coefficientP;
    }

    /**
     * Set name of township.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set number of people into township.
     * Calculate CoeficientV.
     *
     * @param population
     */
    public void setPopulation(int population) {
        this.population = population;
        this.coefficientV = (int) (this.population * (this.pctMoving / 100.00));
    }

    /**
     * Infection description formatted for printing
     *
     * @return String
     */
    public String showInfection() {
        Formatter formatter = new Formatter();
        formatter.format(TEXT_FORMAT_INFECTION,
                this.name,
                String.format("%,d", this.population),
                String.format("%,d", this.infected),
                String.format("%8.4f", this.pctInfected),
                String.format("%,d", this.newInfected),
                String.format("%,d", this.newCommuter)
        );

        return formatter.toString();
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        Formatter formatter = new Formatter();
        formatter.format(TEXT_FORMAT_TOSTRING,
                this.name,
                String.format("%,d", this.population),
                String.format("%,d", this.coefficientV),
                this.coefficientE,
                this.coefficientP
        );

        return formatter.toString();
    }

    /**
     * Update population infected.
     *
     * @param population
     * @param commuter
     */
    public void updateInfected(int population, int commuter) {
        this.newInfected = population;
        this.newCommuter = commuter;
        this.infected += population + commuter;
        if (this.infected > this.population) {
            this.infected = this.population;
        }

        this.pctInfected = this.infected * 100.00 / this.population;
    }
}