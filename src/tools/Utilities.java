/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tools;

import java.util.Random;
import java.util.Scanner;

/**
 * Tools Library.
 * Class with common or general utilities.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - April 2020
 */
public class Utilities {

    private static final String DATE_FORMATTER = "dd/MM/yyyy";

    private static final String ERROR_SCANNER_DATE = "¡Error! Introduce una fecha en el formato: " + DATE_FORMATTER;
    private static final String ERROR_SCANNER_INT = "¡Error! Debe introducir un valor numérico";
    private static final String ERROR_SCANNER_PERIOD = "¡Error! La fecha hasta es inferior a la desde";
    private static final String ERROR_SCANNER_STRING = "¡Error! El texto introducido es demasiado largo";

    private static final String ERROR_STRING = "**ERROR**";

    private static final String TEXT_SCANNER_DATE = "Introduzca la fecha con formato: " + DATE_FORMATTER;
    private static final String TEXT_SCANNER_DATE_FROM = "Introducir fecha (Desde):";
    private static final String TEXT_SCANNER_DATE_TO = "Introducir fecha (Hasta):";
    private static final String TEXT_SCANNER_PERIOD = "Indicar el periodo a listar";

    /**
     * Returns a line string of the indicated length
     *
     * @param character
     * @param size
     * @return String
     */
    public static String getLine(char character, int size) {
        return new String(new char[size]).replace("\0", Character.toString(character));
    }

    /**
     * Gets a random integer number in the range 1 to the maximum value indicated
     *
     * @param minValue
     * @param maxValue
     * @return int
     */
    public static int randomInt(int minValue, int maxValue) {
        Random aleatorio = new Random(System.currentTimeMillis());
        int value = aleatorio.nextInt(maxValue - minValue + 1);
        return value + minValue;
    }

    /**
     * Request a numeric value to the user.If indicated, an informative text is displayed.
     *
     * @param text
     * @param minValue
     * @param maxValue
     * @return int
     */
    public static int requestInt(String text, int minValue, int maxValue) {
        Scanner keyboard = new Scanner(System.in);
        int result = 0;

        do {
            try {
                if (text.length() > 0) {
                    System.out.print(text + " ");
                }

                result = keyboard.nextInt();
                System.out.println("");
            } catch (Exception e) {
                keyboard.next();
                System.out.println(ERROR_SCANNER_INT);
                result = -1;
            }
        } while (
                result < 0
            || (result > maxValue && maxValue > 0)
            || (result > 0 && result < minValue && minValue > 0)
        );
        return result;
    }

    /**
     * Request a string value to the user. If indicated, an informative text is displayed.
     *
     * @param text
     * @param maxLength
     * @return String
     */
    public static String requestString(String text, int maxLength) {
        Scanner keyborad = new Scanner(System.in);
        String result = "";

        do {
            try {
                if (text.length() > 0) {
                    System.out.print(text + " ");
                }

                result = keyborad.nextLine();
                System.out.println("");

                if (maxLength > 0 && result.length() > maxLength) {
                    System.out.println(ERROR_SCANNER_STRING);
                    result = ERROR_STRING;
                }
            } catch (Exception exception) {
                keyborad.next();
                System.out.println(ERROR_SCANNER_INT);
                result = ERROR_STRING;
            }
        } while (result.equals(ERROR_STRING));
        return result;
    }
}
