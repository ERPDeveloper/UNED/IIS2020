# Virus Simulator 2019-2020

The objective of the system is to facilitate the simulation of the spread of a
virus in a populated territory following an exponential model. The population may
be organized into different groups.

However, indicate that given the theoretical nature of the simulation, added to
different variables not contemplated by the application, it could lead to
different results from real cases.

### Pre-requisitos

Developed in Java with JDK 12, Encoding UTF-8


## Licencia

This project is under the License (GNU General Public License v3.0) - look at the file [LICENSE.md](LICENSE.md) for details.
